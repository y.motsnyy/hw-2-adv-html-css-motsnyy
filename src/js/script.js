const headerMenuBtn = document.querySelector('.header-menu');
const navBtn = document.querySelector('.header-nav');

headerMenuBtn.addEventListener('click', () => {
    navBtn.classList.remove('disappear');
    headerMenuBtn.classList.toggle('header-menu--opened');

    if (headerMenuBtn.classList.contains('header-menu--opened')) {
        navBtn.classList.add('appear');
    } else {
        navBtn.classList.remove('appear');
        navBtn.classList.add('disappear');
    }
});

